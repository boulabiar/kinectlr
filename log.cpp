#include "log.h"

string getPath(char* place, int captN, int inst, int surfN, int suiteN) {
    std::stringstream path;
    path << place << "/capture" << captN << "/c" << inst << ".surf" << surfN <<"/s" << suiteN;
    return path.str();
}

void createRecPaths(char* place, int captN, int inst) {
    std::stringstream dirs; //, files0, files1;
    //std::ostringstream surfN;
    dirs << "mkdir -p " << place << "/capture" << captN << "/c" << inst << ".surf1/";
    dirs << " " << place << "/capture" << captN << "/c" << inst << ".surf2/";

    //cout << dirs.str().c_str() << endl;
    system(dirs.str().c_str());
//    surfN << "s" << std::setw(4) << std::setfill('0') << directory;
//    files1 << "~/Tests/userA/capt" << captN << "/1.surf/" << surfN.str() << ".surf";
//    std::stringstream gesturefile;
//    gesturefile << "~/Tests/userA/capt" << captN ;
}

string getTrajPath(string place, int captN, int inst) {
    std::stringstream path;
    path << place << "/capture" << captN << "/c" << inst << ".ges" ;
    return path.str();
}

void writePt(CvPoint p, int z, const char *filename, int when){
    timeval t;
    gettimeofday(&t, 0);
    //time_t t = time(0);
    ofstream myfile;
    myfile.open(filename, ios::out|ios::app);
    if (!when) myfile << endl;
    myfile << p.x << " " << p.y << " " << (maxval-z) << " ";
    myfile.close();
}

// writeSurf function saves a 3d point cloud of a kinect 16bit surface
void writeSurf(IplImage *surf, const char* file){
    //timeval t;
    //gettimeofday(&t, 0);
    ofstream myfile;
    myfile.open(file, ios::out);
    for (int i=0; i< surf->height ; i++)
        for (int j=0; j< surf->width ; j++)
            if (cvGet2D(surf, i , j).val[0]>minval && cvGet2D(surf, i , j).val[0]<maxval)
                myfile << i << "  " << j << "  " << int(maxval-(cvGet2D(surf,i,j).val[0])) << endl;
    myfile.close();
}

void writeContour1(CvBlob const *blob, IplImage* depth, CvPoint handcenter, const char * file)
{
    ofstream myfile;
    CvPoint pt;
    CvContourPolygon *polygon = cvConvertChainCodesToPolygon(&blob->contour);

    myfile.open(file, ios::out);
    myfile << handcenter.x << "  " << handcenter.y << "  " << int(maxval-(cvGet2D(depth,handcenter.y,handcenter.x).val[0])) << endl;
    for (int i=0 ; i<polygon->size()-1 ; ++i) {
        pt.x=polygon->at(i).x;
        pt.y=polygon->at(i).y;
        if ((pt.x > handcenter.x-75) && (pt.x < handcenter.x+75) && (pt.y > handcenter.y-75) && (pt.y > handcenter.y-75))
            myfile << pt.x << "  " << pt.y << "  " << int(maxval-(cvGet2D(depth,pt.y,pt.x).val[0])) << endl;
    }
    myfile.close();
}

void writeContour(CvBlob const *blob, IplImage* depth, CvPoint handcenter, const char * file)
{
    ofstream myfile;
    CvPoint pt;
    CvContourPolygon *polygon = cvConvertChainCodesToPolygon(&blob->contour);
    int diff1=0, diff2=0;
    myfile.open(file, ios::out);
    //myfile << handcenter.x << "  " << handcenter.y << "  " << int(maxval-(cvGet2D(depth,handcenter.y,handcenter.x).val[0])) << endl;
    for (int i=0 ; i<polygon->size()-1 ; ++i) {
        pt.x=polygon->at(i).x;
        pt.y=polygon->at(i).y;
        diff1 = pt.x-handcenter.x;
        diff2 = pt.y-handcenter.y;
        //75*75 = 5625
        if ((diff1*diff1)<3000 && (diff2*diff2)<3000)
            myfile << pt.x << "  " << pt.y << "  " << int(maxval-(cvGet2D(depth,pt.y,pt.x).val[0])) << endl;
    }
    myfile.close();
}

CvContourPolygon* getHandContour(CvBlob const *blob, CvPoint handcenter)
{
    CvPoint pt;
    CvContourPolygon *polygon = cvConvertChainCodesToPolygon(&blob->contour);
    CvContourPolygon *spolygon = cvSimplifyPolygon(polygon, 16.);
    CvContourPolygon *result = new CvContourPolygon;
    int diff1=0, diff2=0;
    for (int i=0 ; i<spolygon->size()-1 ; ++i) {
        pt.x=spolygon->at(i).x;
        pt.y=spolygon->at(i).y;
        diff1 = pt.x-handcenter.x;
        diff2 = pt.y-handcenter.y;
        if ((diff1*diff1)<3000 && (diff2*diff2)<3000)
            result->push_back(pt);
    }
    return result;
}

void writeHand(IplImage *surf, CvPoint c, unsigned int number, const char* file){
    ofstream myfile;
    myfile.open(file, ios::out);
    int x1,x2, y1,y2, size=75;
    x1=max(0,c.x-size);
    y1=max(0,c.y-size);
    x2=min(640,size*2);
    y2=min(480,size*2);

    CvRect roi = cvGetImageROI(surf);
    cvSetImageROI(surf, cvRect(x1, y1, x2, y2));
    for (int i=0; i< size*2; i++)
        for (int j=0; j< size*2; j++)
            if (cvGet2D(surf, i , j).val[0]>minval && cvGet2D(surf, i , j).val[0]<maxval)
                myfile << i << "  " << j << "  " << int(maxval-(cvGet2D(surf,i,j).val[0])) << endl;
    myfile.close();
    cvSetImageROI(surf, roi);
}
