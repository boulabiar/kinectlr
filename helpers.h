#ifndef HELPERS_H
#define HELPERS_H

inline float dist(CvPoint a, CvPoint b) {
    return sqrt( (a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y) );
}

inline float dist3d(CvPoint a, int z1, CvPoint b, int z2) {
    return sqrt( (a.x-b.x)*(a.x-b.x)*7.3 + (a.y-b.y)*(a.y-b.y)*7.3 + (z1-z2)*(z1-z2) );
}

#endif // HELPERS_H
