#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <iomanip>
#include <sstream>
#include <algorithm>
#include <unistd.h>

#include <libfreenect.h>
#include "libfreenect_cv.h"
#include "cvBlob/cvblob.h"

#include "dollar/GeometricRecognizer.h"
#include "dollar/PathWriter.h"

#include "helpers.h"
#include "log.h"
#include "recog.h"
#include "com.h"

//#include "icp/icpPointToPlane.h"
//#include "icp/icpPointToPoint.h"

using namespace std;
using namespace cvb;
using namespace DollarRecognizer;

// Directory where data will be saved
#define RECDIR "/home/boulabiar/Desktop/data"
uint16_t t_gamma[2048];
int maxval=1310;
int minval=440;

void CallBackFunc(int event, int x, int y, int flags, void* userdata)
{
  cout << "Mouse position (" << x << ", " << y << ")" << endl;
}

/////////////////////////////////////////////////////////////////////
IplImage *GlViewMono(IplImage *depth)
{
    static IplImage *image = 0;
    if (!image) image = cvCreateImage(cvSize(640,480), 8, 1);
    unsigned char *depth_mid = (unsigned char*)image->imageData;
    int pval;
    for (int i = 0; i < 640*480; i++) {
        pval = ((uint16_t *)depth->imageData)[i];
        depth_mid[i] = 0;
        if (pval < maxval && pval > minval)
            depth_mid[i] = pval/2047. * 255 ;
    }
    return image;
}

/////////////////////////////////////////////////////////////////////
IplImage *GlViewColor(IplImage *depth)
{
    static IplImage *image = 0;
    if (!image) image = cvCreateImage(cvSize(640,480), 8, 3);
    unsigned char *depth_mid = (unsigned char*)image->imageData;
    float vr, vg, vb, vi, vh, v0, v1;

    int i;
    for (i = 0; i < 640*480; i++) {
        //int pval = t_gamma[depth[i]];
        int pval = ((uint16_t *)depth->imageData)[i];
        // Use the HSL color space to directly map depth to colors, then transform values to RGB
        if (pval>=maxval || pval<=minval) {
            depth_mid[3*i+0] = 0;
            depth_mid[3*i+1] = 0;
            depth_mid[3*i+2] = 0;
        } else {
            vh = 6*pval/2048. ;// *(maxval-minval);
            vi = (int)vh;
            v0 = (vh-vi);
            v1 = 1-v0;
            switch ((int)vi) {
            case 0:  vr=1 ; vg=v0; vb=0 ; break;
            case 1:  vr=v1; vg=1 ; vb=0 ; break;
            case 2:  vr=0 ; vg=1 ; vb=v0; break;
            case 3:  vr=0 ; vg=v1; vb=1 ; break;
            case 4:  vr=v0; vg=0 ; vb=1 ; break;
            default: vr=1 ; vg=0 ; vb=v1;
            }
            depth_mid[3*i+0] = vr * 255 ; // *2048/(maxval-minval)
            depth_mid[3*i+1] = vg * 255 ;
            depth_mid[3*i+2] = vb * 255 ;
        }
    }
    return image;
}
/////////////////////////////////////////////////////////////////////

void cvShowImageHand(const char *window, IplImage *img, CvBlob const *blob, CvPoint c, unsigned int number)
{
    int x1,x2, y1,y2, size=75;
    //  x1=blob->minx;
    //  y1=blob->miny;
    //  x2=blob->maxx-blob->minx;
    //  y2=blob->maxy-blob->miny;

    x1=max(0,c.x-size);
    y1=max(0,c.y-size);
    x2=min(640,size*2);
    y2=min(480,size*2);
    CvRect roi = cvGetImageROI(img);
    cvSetImageROI(img, cvRect(x1, y1, x2, y2));
    cvShowImage(window,  img);
    cvMoveWindow(window, 640+number*128,0);
    cvSetImageROI(img, roi);
}

void drawPinch(IplImage *frame, CvContourPolygon* ct, CvPoint c) {
    CvPoint pt1, pt2, cent;
    CvFont font;
    stringstream ss;
    int size=ct->size();
    cvInitFont(&font, CV_FONT_HERSHEY_TRIPLEX, 1., 1.);

    cent.x=0;
    cent.y=0;
    if (c.x>0 && c.x<640) cent.x=c.x;
    if (c.y>0 && c.y<480) cent.y=c.y;

    // Temporary handling, needs to be rewritten
    if (size>1) {
        pt1.x=ct->at(0).x;
        pt1.y=ct->at(0).y;
        for (int k=1 ; k < size ; k++) {
            if (dist(ct->at(k),c)>dist(pt1,c)) {
                pt2.x=pt1.x;
                pt2.y=pt1.y;
                pt1.x=ct->at(k).x;
                pt1.y=ct->at(k).y;
            }
        }
        //ss << int(dist(pt1, pt2));
        // Temporary condition, needs to be rewritten
        if (int(dist(pt1, pt2))<50 && int(dist(pt1, pt2))>5)
            cvPutText(frame, "- Pinch", cvPoint(c.x,c.y+20), &font, cvScalar(255,200,200));
        //cvCircle(frame, pt1, 5, CV_RGB(255,255,255), 8);
        //cvCircle(frame, pt2, 5, CV_RGB(255,255,255), 8);
    }
       //cvCircle(frame, cent, 5, CV_RGB(255,255,255), 8);
    //cout << dist(pt1, pt2) << endl;
}

bool drawPinch2(IplImage* depth, CvPoint c, IplImage * frame){
    int x1,x2, y1,y2, size=70;
    bool rval=false;
    int minv=6000,maxv=0;
    CvFont font;
    cvInitFont(&font, CV_FONT_HERSHEY_SIMPLEX, 1., 1.);

    x1=max(0,c.x-size);
    y1=max(0,c.y-size);
    x2=min(640,size*2);
    y2=min(480,size*2);

    CvRect roi = cvGetImageROI(depth);
    cvSetImageROI(depth, cvRect(x1, y1, x2, y2));
    for (int i=0; i< size*2; i++)
        for (int j=0; j< size*2; j++)
            if (cvGet2D(depth, i , j).val[0]>minval && cvGet2D(depth, i , j).val[0]<maxval) {
                minv=min( int(maxval-(cvGet2D(depth,i,j).val[0])), minv );
                maxv=max( int(maxval-(cvGet2D(depth,i,j).val[0])), maxv );
            }
    cvSetImageROI(depth, roi);
    //cout << "min " << minv << " max " << maxv << " == DIFF " << maxv-minv << endl;

    if (maxv-minv<100) {
        cvPutText(frame, "- Pinch ", cvPoint(c.x,c.y+20), &font, cvScalar(255,255,255));
        rval=true;
    }
        //cout << "- Pinch " << maxv-minv << endl;
    return rval;
}

/////////////////////////////////////////////////////////////////////
void cvShowImageBlob(const char *window, IplImage *img, CvBlob const *blob, unsigned int number)
{
    CvRect roi = cvGetImageROI(img);
    cvSetImageROItoBlob(img, blob);
    cvShowImage(window,  img);
    cvMoveWindow(window, 640+number*128,0);
    cvSetImageROI(img, roi);
}
/////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////
int main()
{

    ////////////////////////////////////
    /// \brief socket code !
    /////////////////////////

      int listenfd = 0, connfd = 0;
      struct sockaddr_in serv_addr;

      char sendBuff[25];
      char recvBuff[15];
      time_t ticks;

      listenfd = socket(AF_INET, SOCK_STREAM, 0);
      memset(&serv_addr, '0', sizeof(serv_addr));
      memset(sendBuff, '0', sizeof(sendBuff));

      serv_addr.sin_family = AF_INET;
      serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
      serv_addr.sin_port = htons(5508);

      bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));

      //listen(listenfd, 1); //////////////////////////////////

      while (!connfd)
          connfd = accept(listenfd, (struct sockaddr*)NULL, NULL);

      int flag = 1;
      int result = setsockopt(connfd, IPPROTO_TCP, TCP_NODELAY, (char *) &flag, sizeof(int));

    ///////////////////////////////

    static IplImage *frame = cvCreateImage(cvSize(640,480), 8, 3);
    static IplImage *frameC = cvCreateImage(cvSize(640,480), 8, 3);
    static IplImage *segmentated = cvCreateImage(cvSize(640,480), 8, 1);
//    static IplImage *oldHand = cvCreateImage(cvSize(150,150), 16, 1);
//    static IplImage *newHand = cvCreateImage(cvSize(150,150), 16, 1);

    CvTracks tracks;
    //IplConvKernel* morphKernel = cvCreateStructuringElementEx(4, 4, 1, 1, CV_SHAPE_ELLIPSE, NULL);
    int captN=0, instanceN=0;

    cvNamedWindow("Right", 0);

    cvNamedWindow("Depth", 0); cvMoveWindow("Depth", 0,0);
    cv::createTrackbar("level0","Depth", &minval, 1800);
    cv::createTrackbar("level1","Depth", &maxval, 2200);
    //cvSetMouseCallback("Depth", CallBackFunc, NULL);


    CvPoint p0;
    p0.x = p0.y = 0 ;
//    vector<CvPoint> pts (50, p0);
//    vector<int> zs (50, 0);
    int state=0, pinchstate=1, suiteN=0;

    GeometricRecognizer g;
    Path3D recordPath;
    g.addFileTemplate("ges/bounce.ges");
    g.addFileTemplate("ges/vbox.ges");
    g.addFileTemplate("ges/spring.ges");
    g.addFileTemplate("ges/circledeep.ges");
    g.activateTemplates();
    string recName = "none";
    stringstream angleV ;


    char c;
    while ((c = cvWaitKey(10)) && (int(c) != 27)) {
        /*IplImage *image = freenect_sync_get_rgb_cv(0);
        if (!image) {
            printf("Error: Kinect not connected?\n");
            return -1;
        }
        cvCvtColor(image, image, CV_RGB2BGR);*/
        IplImage *depth = freenect_sync_get_depth_cv(0);
        if (!depth) {
            printf("Error: Kinect not connected?\n");
            return -1;
        }

        frame = GlViewColor(depth);
        cvCopy(frame, frameC, NULL);

        segmentated = GlViewMono(depth);
        //cvMorphologyEx(segmentated, segmentated, NULL, morphKernel, CV_MOP_CLOSE, 2);
        IplImage *labelImg = cvCreateImage(cvGetSize(frame), IPL_DEPTH_LABEL, 1);

        CvBlobs blobs;
        cvLabel(segmentated, labelImg, blobs);
        cvFilterByArea(blobs, 2500, 1000000);
        cvRenderBlobs(labelImg, blobs, frame, frame, CV_BLOB_RENDER_BOUNDING_BOX|CV_BLOB_RENDER_ANGLE|CV_BLOB_RENDER_CENTROID);
        cvUpdateTracks(blobs, tracks, 20., 5);
        cvRenderTracks(tracks, frame, frame, CV_TRACK_RENDER_ID|CV_TRACK_RENDER_BOUNDING_BOX);


        write(connfd, sendBuff, strlen(sendBuff));           ///////////////////////////////////
        fd_set read_sd;
        string reconsurveReq;
        // all those actions of setting maxsd to the maximum fd +1 and FDSETing the FDs.
        FD_ZERO(&read_sd);
        FD_SET(connfd, &read_sd);
        if(FD_ISSET(connfd, &read_sd))
        {
            recv(connfd, recvBuff, sizeof(char)*15, MSG_DONTWAIT);
            reconsurveReq = string(recvBuff);
            cout << reconsurveReq << endl;
        }



// State Machine, Space to save, and a to the next gesture
        if ('a'==c) {
            captN++;
            instanceN=0;
        }
        if ('p'==c)
            pinchstate=!pinchstate;

        switch (state) {
        case 0 : //start
            //cout << "state " << state << endl;
            //cout << blobs.size();
            if (32==int(c) || "GESTURE_START"==reconsurveReq){
                state++;
                createRecPaths(RECDIR, captN, instanceN);
                suiteN=0;
            }
            break;

        case 1 : //record
            //cout << "state " << state << endl;
            if (32==int(c) || "GESTURE_START"==reconsurveReq) {
                instanceN++;
                state++;
            } break;

        case 2 : //end record
            break;
        }

        // Get Hand_1 and Hand_2, to be removed
        unsigned int hand1N=0, hand2N=0;
        unsigned int numberTracks = tracks.size();
        CvTracks::iterator itT=tracks.begin(); //it!=tracks.end(); ++it;
        switch (numberTracks) {
            case 1: hand1N = itT->first; break;
            case 2: hand1N = itT->first; ++itT; hand2N = itT->first;  break;
            default: break;
        }
        CvFont font;
        cvInitFont(&font, CV_FONT_HERSHEY_SIMPLEX, 1., 1.);
        cvPutText(frame, recName.c_str(), cvPoint(20,20), &font, cvScalar(255,255,255));
        double t=0., farness=0.;
        int xintersect=0, yintersect=0;


        //cvRectangle(frame, cvPoint(450,250), cvPoint(520,350), CV_RGB(255,255,255), 2);
        unsigned int trN = 0;//it->first;
        for (CvTracks::iterator it=tracks.begin(); it!=tracks.end() ; ++it) {
            std::stringstream windowN;
            unsigned int blobN = it->second->label;
            windowN << "track" << trN;
            for (CvBlobs::iterator it=blobs.begin(); it!=blobs.end(); it++)
                if ( it->first == blobN ) {

                    CvPoint pt = getHandCenter(it->second)[0];  // Center Point
                    CvPoint ptM = getHandCenter(it->second)[1]; // Finger Point

                    //cvShowImageBlob(windowN.str().c_str(), frameC, it->second, trN);
                    //cvShowImageHand(windowN.str().c_str(), frameC, it->second, pt, trN);
                    cvCircle(frame, pt, 5, CV_RGB(0,0,255),8);
                    cvCircle(frame, getHandCenter(it->second)[1], 5, CV_RGB(255,255,255),8);
                    if (0==state) {
                        //if (getBlobID(tracks, hand1N) && it->first==getBlobID(tracks, hand1N))
                        {
                            t=(maxval-cvGet2D(depth,pt.y,pt.x).val[0])/(cvGet2D(depth,pt.y,pt.x).val[0]-cvGet2D(depth,ptM.y,ptM.x).val[0]);
                            xintersect=pt.x-(ptM.x-pt.x)*t;
                            yintersect=pt.y-(ptM.y-pt.y)*t;
                            farness=dist3d(ptM, cvGet2D(depth,ptM.y,ptM.x).val[0], cvPoint(xintersect, yintersect), maxval)/maxval;
                            cout << farness << endl;
                            cvCircle(frame, cvPoint(xintersect,yintersect), farness*20, CV_RGB(255,255,255), 8);
                            double zvalV = cvGet2D(depth,ptM.y,ptM.x).val[0];
                            double modV=sqrt( ptM.x*ptM.x + ptM.y*ptM.y + zvalV*zvalV);
                            angleV << acos( (cvGet2D(depth,ptM.y,ptM.x).val[0]) / modV)*180/3.14;
                            cvPutText(frame, angleV.str().c_str(), cvPoint(50,150), &font, cvScalar(255,255,255));
                            angleV.str("");

                            //ticks = time(NULL);                     ////////////////////////////////////////////////
                            snprintf(sendBuff, sizeof(sendBuff), "%.4f,%.4f", (xintersect-135)/390., (yintersect-140)/300.); /////////////////
                            write(connfd, sendBuff, strlen(sendBuff));           ///////////////////////////////////
                        }
                    }
                    if (1==state) {
                        if (getBlobID(tracks, hand1N) && it->first==getBlobID(tracks, hand1N)) {
                            //writeHand(depth, pt, trN, getPath(RECDIR, captN, instanceN, 1, suiteN).c_str());
                            //writeContour(it->second, depth, pt, getPath(RECDIR, captN, instanceN, 1, suiteN).c_str());
                            //icpHands(depth, pt, oldHand);
                            //cout << getPath(RECDIR, captN, instanceN, 1, suiteN).c_str() << endl;
                            suiteN++;
                            //writePt(pt, cvGet2D(depth,ptM.y,ptM.x).val[0], getTrajPath(RECDIR, captN, instanceN).c_str(), 0);
                            recordPath.push_back(Point3D(ptM.x, ptM.y, cvGet2D(depth,ptM.y,ptM.x).val[0]));
                            cvPutText(frame, "Recording..", cvPoint(20,420), &font, cvScalar(255,255,255));


                        } else {
//                            if (pinchstate)
//                                drawPinch(frame, getHandContour(it->second, pt), pt);
//                            else
                            if (drawPinch2(depth, pt, frame))
                                state=1;
                            else
                                state=0;
                            //if (drawPinch2(depth, pt, frame) && pt.x>450 && pt.y>250)
                                //cvPutText(frame, "state 1 -- ", cvPoint(20,20), &font, cvScalar(255,255,255));
                            //writeHand(depth, pt, trN, getPath(RECDIR, captN, instanceN, 2, suiteN).c_str());
                            suiteN++;
//                            gesturefile << "/1.p" << std::setw(2) << std::setfill('0') << captN << ".path";
                            //writePt( pt, cvGet2D(depth,ptM.y,ptM.x).val[0], getTrajPath(RECDIR, captN, instanceN).c_str(), 1);
//                            cout << " p " << gesturefile.str().c_str() << " surf "<< files1.str().c_str() << endl;
                        }
                    }
                    else if (2==state) {
                        RecognitionResult r=g.recognize(recordPath);
                        cout << "Recognized gesture: " << r.name << endl;
                        recName = r.name;
                        //cout << "1$ Score:" << r.score << endl;
                        cout << recName.c_str() << endl;
                        snprintf(sendBuff, sizeof(sendBuff), "REC:%s", recName.c_str()); /////////////////
                        write(connfd, sendBuff, strlen(sendBuff));           ///////////////////////////////////

                        recordPath.clear();

                        state=0;
                    }
                }
            trN++;
       }
        cvShowImage("Right", frame);
        cvReleaseBlobs(blobs);
        cvReleaseImage(&labelImg);
    }
    cvReleaseImage(&segmentated);
    cvReleaseImage(&frame);
    cvReleaseImage(&frameC);
    return 0;
}

//value = ((uchar *)(depth->imageData + pt.y*depth->widthStep))[pt.x*depth->nChannels +0];
/*  // Hull Defects to detect number of fingers
            CvMemStorage* storage = cvCreateMemStorage();
            int hullcount;
            CvPoint pt0;
            CvSeq* ptseq = cvCreateSeq(CV_SEQ_KIND_GENERIC|CV_32SC2, sizeof(CvContour), sizeof(CvPoint), storage);
            CvSeq* hull;
            for(int i = 0; i < sPolygon->size() ; i++ ) {
                pt0.x = sPolygon->at(i).x;
                pt0.y = sPolygon->at(i).y;
                cvSeqPush( ptseq, &pt0 );
            }
            hull = cvConvexHull2( ptseq, 0, CV_CLOCKWISE, 0 );
            hullcount = hull->total;

            CvSeq* SeqDefects = cvConvexityDefects(ptseq, hull);
            CvConvexityDefect *defects = (CvConvexityDefect*)malloc(sizeof(CvConvexityDefect)*SeqDefects->total);
            cvCvtSeqToArray(SeqDefects, defects, CV_WHOLE_SEQ);
            int fThreshold = (it->second->maxy - it->second->miny )/4 ;
            int fings=0;
            for(int i = 0; i < SeqDefects->total ; i++ )
            {
                //  cout <<  " - " << defects[i].depth ;
                if (defects[i].depth > 12.) //fThreshold)
                    //cout << defects[i].depth << endl;
                    fings++;
            }
            //cout << fings << endl;
            //fings+(SeqDefects->total > 0);

    /// Hull DefectsEND
************************************/
