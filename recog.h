#ifndef RECOG_H
#define RECOG_H

#include "cvBlob/cvblob.h"

unsigned int getBlobID(cvb::CvTracks tracks, unsigned int trackID);
std::vector<CvPoint> getHandCenter(cvb::CvBlob const *blob);

#endif // RECOG_H
