#ifndef LOG_H
#define LOG_H

#include "cvBlob/cvblob.h"
#include <fstream>
#include <iostream>
#include <string>
//#include <ctime>
#include <sys/time.h>

using namespace cvb;
using namespace std;

extern int maxval;
extern int minval;

string getPath(char* place, int captN, int inst, int surfN, int suiteN);
void createRecPaths(char* place, int captN, int inst);
string getTrajPath(string place, int captN, int inst);
void writePt(CvPoint p, int z, const char *filename, int when);

// writeSurf function saves a 3d point cloud of a kinect 16bit surface
void writeSurf(IplImage *surf, const char* file);

void writeContour1(CvBlob const *blob, IplImage* depth, CvPoint handcenter, const char * file);
void writeContour(CvBlob const *blob, IplImage* depth, CvPoint handcenter, const char * file);
CvContourPolygon* getHandContour(CvBlob const *blob, CvPoint handcenter);
void writeHand(IplImage *surf, CvPoint c, unsigned int number, const char* file);

#endif // LOG_H
