################################

TEMPLATE = app
#TARGET =
#DEPENDPATH += .
INCLUDEPATH += .
INCLUDEPATH += /usr/include /usr/local/include/libfreenect /usr/include/libusb-1.0

# Input
HEADERS +=  libfreenect_cv.h \
            libusb.h \
            c_sync/libfreenect_sync.h \
            cvBlob/cvblob.h \
    dollar/SampleGestures.h \
    dollar/PathWriter.h \
    dollar/GestureTemplate.h \
    dollar/GeometricRecognizerTypes.h \
    dollar/GeometricRecognizer.h \
    icp/matrix.h \
    icp/kdtree.h \
    icp/icpPointToPoint.h \
    icp/icpPointToPlane.h \
    icp/icp.h \
    com.h \
    log.h \
    helpers.h \
    recog.h

SOURCES +=  libfreenect_cv.c \
            c_sync/libfreenect_sync.c \
            cvBlob/cvtrack.cpp \
            cvBlob/cvlabel.cpp \
            cvBlob/cvcontour.cpp \
            cvBlob/cvcolor.cpp \
            cvBlob/cvblob.cpp \
            cvBlob/cvaux.cpp \
            cvdemo.cpp \
    dollar/GeometricRecognizer.cpp \
    icp/matrix.cpp \
    icp/kdtree.cpp \
    icp/icpPointToPoint.cpp \
    icp/icpPointToPlane.cpp \
    icp/icp.cpp \
    icp/demo.cpp \
    recog.cpp \
    helpers.cpp \
    log.cpp

LIBS += -L/usr/local/lib64 \
    -lfreenect \

LIBS += -L/usr/lib/ \
    -lopencv_ml \
    -lopencv_core \
    -lopencv_highgui \
    -lopencv_imgproc \
    -lopencv_legacy
