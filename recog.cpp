#include "recog.h"
#include "helpers.h"
#include <vector>

using namespace cvb;
using namespace std;

unsigned int getBlobID(CvTracks tracks, unsigned int trackID) {
    unsigned int blobID=0;
    for (CvTracks::iterator it=tracks.begin(); it!=tracks.end() && !blobID ; ++it)
        if ( it->second->id == cvb::trackID )
            blobID = it->second->label;
    //cout << " Track: " << trackID << "Blob: " << blobID << endl;
    return blobID;
}

/////////////////////////////////////////////////////////////////////
//void icpHands(IplImage* depth, CvPoint pt, IplImage* oldHand) {}

//CvTrack (id, label, ..)
//               |
//         -------
//         v
//CvBlob (id)
/////////////////////////////////////////////////////////////////////

vector<CvPoint> getHandCenter(CvBlob const *blob)
{
    CvContourPolygon *polygon = cvConvertChainCodesToPolygon(&blob->contour);
    CvContourPolygon *sPolygon = cvSimplifyPolygon(polygon, .4);
//    CvContourPolygon *convPoly = cvPolygonContourConvexHull(sPolygon);
//    cvRenderContourPolygon(sPolygon, frame, CV_RGB(0, 255, 255));
//    cvRenderContourPolygon(convPoly,frame, CV_RGB(255, 0, 255));

    int size;
    CvPoint pt, pti, centroid, center;
    vector<CvPoint> returnPts ;
    //CvBlob *blob=blb;
    centroid.x = blob->centroid.x;
    centroid.y = blob->centroid.y;
    center.x = 320;
    center.y = 240;
    size=sPolygon->size();

    pt.x=sPolygon->at(0).x;
    pt.y=sPolygon->at(0).y;
    for (int k=0 ; k < size ; k++) {
        pti.x=sPolygon->at(k).x;
        pti.y=sPolygon->at(k).y;
        if (dist(pt,centroid) < dist(pti,centroid) && dist(pt,center) > dist(pti,center)) {
            pt.x=pti.x;
            pt.y=pti.y;
        }
    }

    //detect the farthest points << To be modified as function
    CvPoint ptA, ptB, ptF1, ptF2;
    ptF1.x=sPolygon->at(0).x; ptF1.y=sPolygon->at(0).y;
    ptF2.x=sPolygon->at(1).x; ptF2.y=sPolygon->at(1).y;

    for (int k=0 ; k < size-1 ; k++) {
        for (int kk=1 ; kk < size ; kk++) {
            ptA.x=sPolygon->at(k).x; ptA.y=sPolygon->at(k).y;
            ptB.x=sPolygon->at(kk).x; ptB.y=sPolygon->at(kk).y;
            if (dist(ptA,ptB) > dist(ptF1, ptF2)) {
                ptF1.x=ptA.x; ptF1.y=ptA.y;
                ptF2.x=ptB.x; ptF2.y=ptB.y;
            }
        }
    }
    // F1 and F2 are the two farthest points


    //select a point between the two ends, with a weight w
    float w=0.85;
    if (dist(ptF1,center) < dist(ptF2, center)) {
        ptA.x = w*ptF1.x + (1-w)*ptF2.x ;
        ptA.y = w*ptF1.y + (1-w)*ptF2.y ;
        returnPts.push_back(ptF1);
    } else {
        ptA.x = w*ptF2.x + (1-w)*ptF1.x ;
        ptA.y = w*ptF2.y + (1-w)*ptF1.y ;
        returnPts.push_back(ptF2);
    }

    // select hand.
    CvContourPolygon *handpts = new CvContourPolygon;
    for (int k=0 ; k < size ; k++)
        if (dist(sPolygon->at(k),ptA) < 80) {
             handpts->push_back(sPolygon->at(k));
        }
    //CvContourPolygon *convHand = cvPolygonContourConvexHull(handpts);
    //cvRenderContourPolygon(convHand, frame, CV_RGB(0, 255, 255));
    ptB.x=ptB.y=0;
    for (unsigned int k=0 ; k < handpts->size() ; k++) {
        ptB.x+=handpts->at(k).x;
        ptB.y+=handpts->at(k).y;
    }
    ptB.x/=handpts->size();
    ptB.y/=handpts->size();

    returnPts.insert(returnPts.begin(),ptB);
    return returnPts;
}
