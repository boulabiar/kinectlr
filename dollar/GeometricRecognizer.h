#ifndef _GeometricRecognizerIncluded_
#define _GeometricRecognizerIncluded_

#include <limits>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include "GeometricRecognizerTypes.h"
#include "GestureTemplate.h"
#include "SampleGestures.h"

using namespace std;

namespace DollarRecognizer
{
	class GeometricRecognizer
	{
        protected:
		//--- These are variables because C++ doesn't (easily) allow
		//---  constants to be floating point numbers
		double halfDiagonal;
		double angleRange;
		double anglePrecision;
		double goldenRatio;

		//--- How many points we use to define a shape
		int numPointsInGesture;
		//---- Square we resize the shapes to
		int squareSize;
		
		bool shouldIgnoreRotation;

                //--- All templates saved in the library
                GestureTemplates allTemplates;
                //--- What we match the input shape against (sub part of allTemplates)
                GestureTemplates templates;

	public:
		GeometricRecognizer();

                int addTemplate(string name, Path3D points);
                int addFileTemplate(string filename);
                DollarRecognizer::Cube boundingBox(Path3D points);
                Point3D centroid(Path3D points);
                double getDistance(Point3D p1, Point3D p2);
		bool   getRotationInvariance() { return shouldIgnoreRotation; }
		double distanceAtAngle(
                        Path3D points, GestureTemplate aTemplate, double rotation);
                double distanceAtBestAngle(Path3D points, GestureTemplate T);
                Path3D normalizePath(Path3D points);
                double pathDistance(Path3D pts1, Path3D pts2);
                double pathLength(Path3D points);
                RecognitionResult recognize(Path3D points, string method="goldenSearch");
                Path3D resample(Path3D points);
                Path3D rotateBy(Path3D points, double rotation);
                Path3D rotateToZero(Path3D points);
                Path3D scaleToSquare(Path3D points);
		void   setRotationInvariance(bool ignoreRotation);
                Path3D translateToOrigin(Path3D points);
                vector<double> vectorize(Path3D points); // for Protractor
                double optimalCosineDistance(vector<double> v1, vector<double> v2); // for Protractor


                void loadTemplates();
                void activateTemplates(const string list[]);
                void activateTemplates();
        private:
                bool inTemplates(string, const string[]);

	};
}
#endif
