#ifndef _GeometricRecognizerTypesIncluded_
#define _GeometricRecognizerTypesIncluded_

/*
* This code taken (and modified) from the javascript version of:
* The $1 Gesture Recognizer
*
*		Jacob O. Wobbrock
* 		The Information School
*		University of Washington
*		Mary Gates Hall, Box 352840
*		Seattle, WA 98195-2840
*		wobbrock@u.washington.edu
*
*		Andrew D. Wilson
*		Microsoft Research
*		One Microsoft Way
*		Redmond, WA 98052
*		awilson@microsoft.com
*
*		Yang Li
*		Department of Computer Science and Engineering
* 		University of Washington
*		The Allen Center, Box 352350
*		Seattle, WA 98195-2840
* 		yangli@cs.washington.edu
*/
#include <math.h>
#include <string>
#include <list>
#include <vector>

using namespace std;

namespace DollarRecognizer
{
        class Point3D
	{
	public:
		//--- Wobbrock used doubles for these, not ints
		//int x, y;
                double x, y, z;
                Point3D()
		{
			this->x=0; 
			this->y=0;
                        this->z=0;
		}
                Point3D(double x, double y, double z)
		{
			this->x = x;
			this->y = y;
                        this->z = z;
		}
	};

        typedef vector<Point3D>  Path3D;
        typedef Path3D::iterator Path3DIterator;

        class Cube
	{
	public:
                double x, y,z, width, height, depth;
                Cube(double x, double y, double z, double width, double height, double depth)
		{
			this->x = x;
			this->y = y;
                        this->z = z;
			this->width = width;
			this->height = height;
                        this->depth = depth;
		}
	};

	class RecognitionResult
	{
	public:
		string name;
		double score;
		RecognitionResult(string name, double score)
		{
			this->name = name;
			this->score = score;
		}
	};
}
#endif
