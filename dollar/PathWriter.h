#ifndef _PathWriterIncluded_
#define _PathWriterIncluded_

#include <fstream>
#include <string>
#include "GeometricRecognizerTypes.h"

using namespace std;

namespace DollarRecognizer
{
	class PathWriter
	{
	public:
		static bool writeToFile(
                        Path3D path,
			const string fileName    = "savedPath.txt", 
			const string gestureName = "DefaultName")
		{
			fstream file(fileName.c_str(), ios::out);

                        file << "Path3D getGesture" << gestureName << "()" << endl;
			file << "{" << endl;
                        file << "\t" << "Path3D path;" << endl;
			
                        Path3D::const_iterator i;
			for (i = path.begin(); i != path.end(); i++)
			{
                Point3D point = *i;
                file << "\t" << "path.push_back(Point3D(" << point.x << "," << point.y << "," << point.z << "));" << endl;
			}

			file << endl;
			file << "\t" << "return path;" << endl;
			file << "}" << endl;

			file.close();

			return true;
		}

        static bool cleanWriteToFile(Path3D path,   const string fileName    = "savedPath",
                                                    const string gestureName = "DefaultName")
        {
            fstream file(fileName.c_str(), ios::out);
            file << gestureName << endl;
            Path3D::const_iterator i;
            for (i = path.begin(); i != path.end(); i++)
            {
                Point3D point = *i;
                file << point.x << " " << point.y << " " << point.z << endl;
            }
            file.close();
            return true;
        }

	};
}

#endif
